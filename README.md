# Pi-hole

This details to how to set up and configure Pi-hole in a Docker container on a Raspberry Pi operating in a network with a BT Smart Hub 2 or Virgin Media Hub 3 router. BT and Virgin hardware is beautifully inflexible, so the Pi-hole also has to operate as a DHCP server and play nicely with IPv6. 

There are two ways to do this: Pi-hole running natively or containerized. Unfortunately the currently available Docker images do not place nicely with the Pi Zero W's architecture, so it is best to set this device up as single use with a native installation.

Parts:


- [Raspberry Pi Zero W](https://thepihut.com/products/raspberry-pi-zero-w)
- [16 gb Micro SD Card](https://www.amazon.co.uk/gp/product/B073K14CVB/ref=ppx_yo_dt_b_asin_title_o02_s01?ie=UTF8&psc=1)
- [USB OTG to Ethernet Adaptor](https://www.amazon.co.uk/gp/product/B00RM3KXAU/ref=ppx_yo_dt_b_asin_title_o02_s02?ie=UTF8&psc=1)

Or:

- [Raspberry Pi 4 Model B](https://thepihut.com/products/raspberry-pi-4-model-b)
- [16 gb Micro SD Card w/ NOOBS](https://thepihut.com/collections/raspberry-pi-sd-cards-and-adapters/products/noobs-preinstalled-sd-card)

And:

- Ethernet Cable
- Micro USB Power Cable

## Raspberry Pi Zero W

This device will only be used to run Pi-hole and it works best when powered directly from the USB port on the router. The initial configuration is a little more fiddly given NOOBS does not permit SSH to be enabled when installing the operating system.

### 1. Booting up the Raspberry Pi

First insert a Micro SD card - at least 8gb in size - into your primary machine. It may prompt you to format the drive, ignore this. Next, download and install the appropriate [Raspberry Pi Imager](https://www.raspberrypi.org/software/) tool for your operating system.

Launch this software and select:

> **CHOOSE OS -> Raspberry Pi OS (other) -> Raspberry Pi OS Lite (32-bit)**

Once you have selected this operating system, select your Micro SD card with:

> **CHOOSE SD CARD**

Then,

> **WRITE**

Allow the software to run to completion and safely eject the Micro SD card from your machine. Now, insert it once again to configure the boot settings, once again ignoring any prompts to formant, fix or scan the drive. Navigate to the `boot` partition and create a blank file called `ssh` - with no extension - and another one called `wpa_supplicant.conf`. Open this second file and insert the following content,

```
country=gb
update_config=1
ctrl_interface=/var/run/wpa_supplicant

network={
 ssid="YOUR NETWORK SSID"
 psk="YOUR NETWORK PASSWORD"
 key_mgmt=WPA-PSK
}
```

Again, safely eject the Micro SD card from your machine and put it into the Raspberry Pi Zero W, plug a Micro USB into to power the Zero - in the case the BT Smart Hub 2 there is a USB-A port on the back of the router which can be used. Allow the Raspberry Pi up to two minutes to finish booting up. A little green LED will light up and, once finished, remain on continuously.

### 2. Configuring passwordless SSH access

Now the Raspberry Pi has booted up, confirm that SSH access is enabled:

```
ssh pi@raspberrypi
```

Follow the prompts and enter the default password for the `pi` user - `raspberry`. It will be suggested to set a new, secure password for the device, do this by typing

```
passwd
```

Follow the on screen prompts to set the new password.

Now, change the default hostname to something more unique and memorable: you may have multiple Raspberry Pis or just desire something shorter to type when SSHing in to configure the device.

```
sudo raspi-config
```

> ***System Options -> Hostname***

Since this device will have a single purpose, my chosen new hostname was simply `pi-hole`. Follow the prompt and reboot your newly named device and reconnect via SSH.

```
ssh pi@<Raspberry-Pi-Hostname>
```

Enter the new password for the `pi` user. In order to avoid having to enter a password every time we want to modify or debug the device, configure passwordless SSH access using keys.

In a new administrator terminal window on your primary machine, first check whether there are already keys which can be used.

```
cd C:/Users/<Username>/.ssh
dir
```
If the files `id_ed25519` and `id_ed25519.pub` do not appear, run `ssh-keygen -t ed25519 -a 100` and set a password to encrypt the private key by following the prompts. Next start the `ssh-agent` service and load the private key into it.

```
ssh-agent -StartupType AutomaticStart-Service ssh-agent
ssh-add id_ed25519
```

Now it is necessary to add the public key to the Raspberry Pi. First confirm that the `.ssh` directory exists

```
mkdir ~/.ssh
```

Next back in the administrator terminal secure copy the public key to this directory on the Raspberry Pi.

```
scp id_ed25519.pub pi@<Raspberry-Pi-Hostname>:~\.ssh\authorized_keys
```

Now test it by closing the SSH terminal window and reconnecting to the Raspberry Pi, hopefully without a password prompt.

### 3. Enabling ethernet connectivity

With passwordless SSH working, plug the USB To Go to Ethernet into the Raspberry Pi and the router, wait a few minutes for it to be automatically configured by the Raspberry Pi. Download and install [Angry IP Scanner](https://angryip.org/) and scan your local network, sort by hostname and if everything is working as expected there will be two entries for your device:

```
<Raspberry-Pi-Hostname>.lan
<Raspberry-Pi-Hostname>.local
```

### 4. Assign static IPs to the Raspberry Pi

For Pi-hole to work correctly in the event of a reboot, container restart or any other disconnection, the Raspberry Pi requires a static IP. 

Since the Raspberry Pi will be configured to replace the DHCP and DNS Server in the router, start by investigating the current settings for the network. Log in to the router in your favourite browser - by default for the BT Smart Hub 2 `192.168.1.254` and the Virgin Media Hub 3 `192.168.0.1`.

When the Pi-hole is eventually configured as a DHCP server, it must not be occupying an IP address within the address pool of said server. So for ease configure the network with the settings already used.

For the BT Smart Hub 2:

> **Advanced Settings -> My network -> IPv4 configuration**

Under **DHCP server** note the _Server address range_ - for my router this was `192.168.1.64 - 192.168.1.253` and the _Lease time_.

For the Virgin Media Hub 3:

> **Advanced Settings -> DHCP -> DHCPv4 server**

Note the _Starting local address_, _Number of CPEs_ and the _Lease time_.

Open AngryIP again and scan your network to find an unoccupied IP address outside of the current DHCP server range. I settled on `192.168.X.50` and `192.168.X.51` for the sake of neatness. 

The Pi-hole will work better when connected to the router via an ethernet connection, but assign a static IP to both the ethernet and wireless interfaces for completeness

On the Raspberry Pi open a terminal via SSH.

```
sudo nano /etc/dhcpcd.conf
```

This will open the editor (with root access) for the DHPC configuration file, scroll to the very bottom and enter the following lines for ethernet:

```
interface eth0
static ip_address=192.168.X.50 <Raspberry-Pi-Static-IP 1>
static routers=192.168.X.X <Router-IP>
```

And optionally for wireless:

```
interface wlan0
static ip_address=192.168.X.51 <Raspberry-Pi-Static-IP 2>
static routers=192.168.X.X <Router-IP>
```
To ensure the changes take place:

```
sudo reboot
```

### 5. Installing and configuring Pi-hole

There is more information on installation available [here](https://github.com/pi-hole/pi-hole/#one-step-automated-install) but the easiest method is to simply run

```
curl -sSL https://install.pi-hole.net | bash
```

And allow the installer to run to completion. It appears to fall over multiple times but this is ok, be patient it should take about 20 minutes. Once complete it is time to configure the software.

Select an upstream DNS - Google is fine - and make sure to install the web interface and any required packages for its operation. No need to worry about setting the static address, we did that earlier. 

When the installer completes it will provide the password for the web interface; do not worry about recording this, the next step is going to be changing it immediately.

```
sudo pihole -a -p
```

Enter and confirm a memorable password for the web interface and then navigate to `192.168.1.50/admin` to log in.

The next section is of this guide about how to get to this point using a containerized implementation on a more powerful Raspberry Pi so go ahead and skip to the end to set up Pi-hole to play nicely with the BT Smarthub 2.

## Raspberry Pi 4 Model B

With a powerful device like the Raspberry Pi 4, it is common for it be running multiple services - media server, emulator, etc. So, to keep everything neat and easily reproducible, Pi-hole can be run containerized in Docker and [Watchtower](https://github.com/containrrr/watchtower) can be configured to monitor all the containers and keep them updated. To avoid conflicts, the Pi-hole container is connected to a `macvlan` network running on the Docker daemon.

> Some applications, especially legacy applications or applications which monitor network traffic, expect to be directly connected to the physical network. In this type of situation, you can use the macvlan network driver to assign a MAC address to each container’s virtual network interface, making it appear to be a physical network interface directly connected to the physical network.

This new network is restricted to a single IP address so it does not change whenever the container recreated and to ensure that it does not interfere with the Pi-hole DHCP server.

In order to keep the Raspberry Pi OS clean and to minimise required packages and file transfers, it is configured remotely using a Windows desktop machine.

### 1. Configure the Raspberry Pi

Follow the steps above to configure passwordless SSH access and assign static IP addresses to the device.

### 2. Install [Docker](https://docs.docker.com/get-docker/)

#### a) Primary Machine

In order to run Docker commands from a terminal on the primary machine, first install Docker. This is straightforward, althought slightly more complex if running basic Windows 10 Home - detailed documentation for this OS can be found [here](https://docs.docker.com/docker-for-windows/install-windows-home/).

Once Docker is installed and, ostensibly, running, validate that it is working with a simple command in your favourite administrator terminal: 

```
docker run hello-world
```

Which should be followed by,

```
Hello from Docker!
This message shows that your installation appears to be working correctly.`
```

#### b) Raspberry Pi

It is always best, that before installing any new package, that you update and upgrade; this ensures that the latest version will be installed.

Open a terminal and run the command:

```
sudo apt-get update && sudo apt-get upgrade
```

Download and execute the installation script:

```
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

For ease of use, add the default **pi** user to the Docker group to allow it to execute commands without the need for the `sudo` prefix.

```
sudo usermod -aG docker pi
```

To ensure the changes take place:

```
sudo reboot
```

Once the system has rebooted, open up the terminal again and again execute:

`docker run hello-world`

If you see the dialogue message then everything is ready to go.

### 3. Run Pi-hole

From now on every command will be executed on the primary machine but will actually control the Raspberry Pi as a remote host. So first set an environment variable called `DOCKER_HOST`.

```
set DOCKER_HOST=ssh://pi@<Raspberry-Pi-Static-IP>
``` 
Alternatively create a Docker context if there is a chance you will be switching between multiple remote hosts.

```
docker context create pi0 --docker "host=ssh://pi@<Raspberry-Pi-Static-IP>
```

All Docker commands will now be run against the Raspberry Pi's Docker environment. Since this uses SSH, it is really good that passwordless SSH access has already been configured.

The official guide to running Pi-hole in Docker gets you to specify ports, this works completely fine if Pi-hole isn't also running as a DHCP server so in this instance use the `docker-compose.yml` from this repo. This container is set up to run in host mode, as in bridge mode you'll be left without a functioning DHCP server.

Start by creating the `macvlan` network the Pi-hole will be connected to, with the single IP range `192.168.1.49`. More information can be found [here](http://tonylawrence.com/posts/unix/synology/free-your-synology-ports/).

```
networks:
  dedicated_ip:
    driver: macvlan
    driver_opts:
      parent: eth0
    ipam:
      config:
        - subnet: X.X.X.0/24
          gateway: X.X.X.X              
          ip_range: X.X.X.49/32
```
Replace the `X.X.X.n` portion with the values from your own home network and leave the numbers after the `/` be - these are subnet masks. *gateway* should be the address of your router and for *ip_range* enter the static IP chosen earlier - in my case `192.168.1.49`.

Navigate to the directory with the `docker-compose.yml` file, open your favourite text editor and edit the following lines:

```
    environment:
      TZ: 'Europe/London' 
      WEBPASSWORD: 'password'
```
Enter your timezone and a secure password of your choice.

Given that Docker and remote hosts do not play nicely with generic volume paths it might be necessary to edit the following lines in line with the your Raspberry Pi. If using generics/wildcards, Docker will first expand that path on the primary machine and then try to pass that explicit path to the remote host.

```
    # Volumes store your data between container upgrades
    volumes:
      - '/home/pi/etc-pihole/:/etc/pihole/'
      - '/home/pi/etc-dnsmasq.d/:/etc/dnsmasq.d/'
```

Once edited, run:

```
docker-compose up -d
```

By default `docker-compose` prepends the folder name of the file to the network as it is created, **dedicated_ip** becomes **pihole-btsmarthub_dedicated_ip**.

Use `docker ps` to check the container is running.

## Configure with Existing Hardware

Once Pi-hole is running, log in to both via web browser `{raspberry-pi-local-ip}/admin` and `{router-local-ip}`. First, disable the DHCP server for both IPv4 and IPv6 on the BT Smart Hub before configuring and starting the one on the Pi-hole. **There needs to be a single DHCP server in a simple home network, otherwise you're in trouble.**

(When doing this first time round I ended up with zero which is possibly worse than more than one, I don't plan on finding out.)

### BT Smart Hub 2

> **Advanced Settings -> My network -> IPv4 configuration -> Enabled -> No**

> **Advanced Settings -> IPv6 -> Configuration -> IPv6 address allocation -> Allocation mode -> Off**

### Virgin Media Hub 3

> **Advanced Settings -> DHCP -> DHCPv4 server -> Disabled**

Now the Pi-hole can be safely enabled, so on the Pi-hole admin configuration webpage.

> **Settings -> DNS**

Enable your DNS servers of choice and choose the interface listening behaviour - ethernet or both.

> **Settings -> DHCP Settings -> DHCP server enabled**

Enter the IP range found right at the start of this guide, the lease time and your router's IP address.

Next check **Enable IPv6 support (SLAAC + RA)** and **Enable DHCP rapid commit (fast address assignment)**.

This should get the Pi-hole up and running, wait a day or so for IP reassignment and caching to catch up with the new settings. The final thing is to add your preferred blocklists.

> **Group Management -> Adlists**

https://dbl.oisd.nl provides c. 1 million unique entries and is a good catch all initially and https://firebog.net/ provides a collection of well-documented, more specialised lists.

